<!-- markdownlint-disable -->

<a href="https://gitlab.inria.fr/askuric/pycapacity_human/-/blob/master/pycapacity_human.py#L0"><img align="right" style="float:right;" src="https://img.shields.io/badge/-source-cccccc?style=flat-square"></a>

# <kbd>module</kbd> `pycapacity_human`





---

<a href="https://gitlab.inria.fr/askuric/pycapacity_human/-/blob/master/pycapacity_human.py#L13"><img align="right" style="float:right;" src="https://img.shields.io/badge/-source-cccccc?style=flat-square"></a>

## <kbd>function</kbd> `polytope_joint_torques`

```python
polytope_joint_torques(N, F_min, F_max, tol=1e-15)
```

A function calculating the polytopes of achievable joint torques based on the moment arm matrix N : 

t = N.F st F_min <= F <= F_max 



**Args:**
 
 - <b>`N`</b>:  moment arm matrix 
 - <b>`F_min`</b>:  minimal muscular forces (passive forces or 0) 
 - <b>`F_max`</b>:  maximal isometric forces  
 - <b>`tolerance`</b>:  tolerance for the polytope calculation 



**Returns:**
 
 - <b>`t_vert`</b> (array):   list of torque vertices 
 - <b>`H`</b> (array):   half-space rep matrix H - H.t < d 
 - <b>`d`</b> (array):   half-space rep vector d 


---

<a href="https://gitlab.inria.fr/askuric/pycapacity_human/-/blob/master/pycapacity_human.py#L34"><img align="right" style="float:right;" src="https://img.shields.io/badge/-source-cccccc?style=flat-square"></a>

## <kbd>function</kbd> `polytope_acceleration`

```python
polytope_acceleration(J, N, M, F_min, F_max, tol=1e-15)
```

A function calculating the polytopes of achievable accelerations based on the jacobian matrix J, moment arm matrix N and mass matrix M 

a = ddx = J.M^(-1).N.F st F_min <= F <= F_max 



**Args:**
 
 - <b>`J`</b>:  jacobian matrix 
 - <b>`N`</b>:  moment arm matrix 
 - <b>`M`</b>:  mass matrix 
 - <b>`F_min`</b>:  minimal muscular forces (passive forces or 0) 
 - <b>`F_max`</b>:  maximal isometric forces  
 - <b>`tolerance`</b>:  tolerance for the polytope calculation 



**Returns:**
 
 - <b>`a_vert`</b> (array):   list of acceleraiton vertices 
 - <b>`H`</b> (array):   half-space rep matrix H - H.a < d 
 - <b>`d`</b> (array):   half-space rep vectors d 


---

<a href="https://gitlab.inria.fr/askuric/pycapacity_human/-/blob/master/pycapacity_human.py#L57"><img align="right" style="float:right;" src="https://img.shields.io/badge/-source-cccccc?style=flat-square"></a>

## <kbd>function</kbd> `polytope_force`

```python
polytope_force(J, N, F_min, F_max, tol)
```

A function calculating the polytopes of achievable foreces based  on the jacobian matrix J and moment arm matrix N 

J^T.f = N.F st F_min <= F <= F_max 



**Args:**
 
 - <b>`J`</b>:  jacobian matrix 
 - <b>`N`</b>:  moment arm matrix 
 - <b>`F_min`</b>:  minimal muscular forces (passive forces or 0) 
 - <b>`F_max`</b>:  maximal isometric forces  
 - <b>`tolerance`</b>:  tolerance for the polytope calculation 



**Returns:**
 
 - <b>`f_vert`</b> (list):   list of cartesian force vertices 
 - <b>`F_vert`</b> (list):   list of muscle force vertiecs 
 - <b>`t_vert`</b> (list):   list of joint torque vertices 
 - <b>`faces`</b> (list):    list of vertex indexes forming polytope faces   


---

<a href="https://gitlab.inria.fr/askuric/pycapacity_human/-/blob/master/pycapacity_human.py#L80"><img align="right" style="float:right;" src="https://img.shields.io/badge/-source-cccccc?style=flat-square"></a>

## <kbd>function</kbd> `extended_convex_hull_method`

```python
extended_convex_hull_method(A, B, y_min, y_max, tol)
```

A function calculating the polytopes of a form: 

z = B.y A.x = z s.t. y_min <= y <= y_max 

or 

A.x = B.y s.t. y_min <= y <= y_max 



**Args:**
 
 - <b>`A`</b>:  matrix 
 - <b>`B`</b>:  matrix 
 - <b>`y_min`</b>:  minimal values 
 - <b>`y_max`</b>:  maximal values 
 - <b>`tol`</b>:  tolerance for the polytope calculation 



**Returns:**
 
 - <b>`x_vert`</b> (list):   list of cartesian force vertices 
 - <b>`y_vert`</b> (list):   list of muscle force vertiecs 
 - <b>`z_vert`</b> (list):   list of joint torque vertices 
 - <b>`faces`</b> (list):    list of vertex indexes forming polytope faces   


---

<a href="https://gitlab.inria.fr/askuric/pycapacity_human/-/blob/master/pycapacity_human.py#L229"><img align="right" style="float:right;" src="https://img.shields.io/badge/-source-cccccc?style=flat-square"></a>

## <kbd>function</kbd> `hyper_plane_shift_method`

```python
hyper_plane_shift_method(A, x_min, x_max, tol=1e-15)
```

Hyper plane shifting method implementation used to solve problems of a form: y = Ax s.t. x_min <= x <= x_max 

Hyperplane shifting method:  *Gouttefarde M., Krut S. (2010) Characterization of Parallel Manipulator Available Wrench Set Facets. In: Lenarcic J., Stanisic M. (eds) Advances in Robot Kinematics: Motion in Man and Machine. Springer, Dordrecht* 



This algorithm can be used to calcualte acceleration polytope, velocity polytoe and even  polytope of the joint achievable joint torques based on the muscle forces 



**Args:**
 
 - <b>`A`</b>:  projection matrix 
 - <b>`x_min`</b>:  minimal values 
 - <b>`x_max`</b>:  maximal values  



**Returns:**
 
 - <b>`H`</b>:  half space representation matrix H - Hx < d 
 - <b>`d`</b>:  half space representaiton vector d - Hx < d 
 - <b>`vertices`</b>:  vertex representation of the polytope 


---

<a href="https://gitlab.inria.fr/askuric/pycapacity_human/-/blob/master/pycapacity_human.py#L299"><img align="right" style="float:right;" src="https://img.shields.io/badge/-source-cccccc?style=flat-square"></a>

## <kbd>function</kbd> `torque_to_muscle_force`

```python
torque_to_muscle_force(N, F_min, F_max, tau, options='lp')
```

A function calculating muscle forces needed to create the joint torques tau 



**Args:**
 
 - <b>`N`</b>:  moment arm matrix 
 - <b>`F_min`</b>:  minimal muscular forces (passive forces or 0) 
 - <b>`F_max`</b>:  maximal isometric forces  
 - <b>`tau`</b>:  joint torque 
 - <b>`options`</b>:  solver type to use: 'lp' - linear programming (defualt), 'qp' - quadratic programming 



**Returns:**
 
 - <b>`F`</b> (list):  list of muscle forces 




---

<a href="https://gitlab.inria.fr/askuric/pycapacity_human/-/blob/master/pycapacity_human.py#L350"><img align="right" style="float:right;" src="https://img.shields.io/badge/-source-cccccc?style=flat-square"></a>

## <kbd>function</kbd> `stack`

```python
stack(A, B, dir='v')
```








---

_This file was automatically generated via [lazydocs](https://github.com/ml-tooling/lazydocs)._
