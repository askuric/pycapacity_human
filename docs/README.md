<!-- markdownlint-disable -->

# API Overview

## Modules

- [`pycapacity_human`](./pycapacity_human.md#module-pycapacity_human)

## Classes

- No classes

## Functions

- [`pycapacity_human.extended_convex_hull_method`](./pycapacity_human.md#function-extended_convex_hull_method): A function calculating the polytopes of a form:
- [`pycapacity_human.hyper_plane_shift_method`](./pycapacity_human.md#function-hyper_plane_shift_method): Hyper plane shifting method implementation used to solve problems of a form:
- [`pycapacity_human.polytope_acceleration`](./pycapacity_human.md#function-polytope_acceleration): A function calculating the polytopes of achievable accelerations
- [`pycapacity_human.polytope_force`](./pycapacity_human.md#function-polytope_force): A function calculating the polytopes of achievable foreces based 
- [`pycapacity_human.polytope_joint_torques`](./pycapacity_human.md#function-polytope_joint_torques): A function calculating the polytopes of achievable joint torques
- [`pycapacity_human.stack`](./pycapacity_human.md#function-stack)
- [`pycapacity_human.torque_to_muscle_force`](./pycapacity_human.md#function-torque_to_muscle_force): A function calculating muscle forces needed to create the joint torques tau


---

_This file was automatically generated via [lazydocs](https://github.com/ml-tooling/lazydocs)._
