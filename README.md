# `pycapacity_human` module

Python module for calculating force capacity based on the human musculoskeletal model 

```python
import pycapacity_human.pycapacity_human as capacity
```

## Module Functions

Polytope calculations:
- [`polytope_acceleration`](./docs/pycapacity_human.md#function-polytope_acceleration): A function calculating the polytopes of achievable accelerations
- [`polytope_force`](./docs/pycapacity_human.md#function-polytope_force): A function calculating the polytopes of achievable foreces based 
- [`polytope_joint_torques`](./docs/pycapacity_human.md#function-polytope_joint_torques): A function calculating the polytopes of achievable joint torques

Base algorithms:
- [`extended_convex_hull_method`](./docs/pycapacity_human.md#function-extended_convex_hull_method): A function calculating the polytopes of a form:
- [`hyper_plane_shift_method`](./docs/pycapacity_human.md#function-hyper_plane_shift_method): Hyper plane shifting method implementation used to solve problems of a form:

Utility methods:
- [`torque_to_muscle_force`](./docs/pycapacity_human.md#function-torque_to_muscle_force): A function calculating muscle forces needed to create the joint torques tau

---